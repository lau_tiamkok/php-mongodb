<?php
// connect to mongodb
$m = new MongoClient();
var_dump($m);

// select a database
$db = $m->mydb;

$db = $m->selectDB("mydb");
var_dump($db);

// Select a collection.
$collection = $db->selectCollection("users");
var_dump($collection);

$document = array(
    "title" => "Mr",
    "fullname" => "John C",
    "username" => "user1",
    "age" => 20
);
$collection->insert($document);

// Find all rows.
$cursor = $collection->find();
var_dump($cursor);
//var_dump(json_encode(iterator_to_array($cursor)));

// Count total.
var_dump($cursor->count());

// Limit.
$cursor->limit(2);
var_dump($cursor->count(true));

// iterate cursor to display title of documents
foreach ($cursor as $key => $document) {

    //var_dump($document);
    if(isset($document["title"])) echo $key. ' = '. $document["username"] . "\n";
}

// Get id of new document.
$id = $document['_id'];

var_dump($id);

// one record.
$result = $collection->findOne(array("_id" => new MongoId($id)));
var_dump($result);

// Remove all.
//$collection->remove();
