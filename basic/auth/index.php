<?php
// connect to mongodb
// Specifying the username and password via the options array (alternative)
$m = new MongoClient("mongodb://localhost", array("db" => "mydb", "username" => "root", "password" => "password"));
var_dump($m);

// select a database
$db = $m->mydb;

$db = $m->selectDB("mydb");
var_dump($db);

// Get current date and time.
// Change the line below to your timezone.
// @ref: http://www.php.net/manual/en/timezones.php
// @ref: http://stackoverflow.com/questions/470617/get-current-date-and-time-in-php
// @ref: http://stackoverflow.com/questions/9541029/insert-current-date-in-datetime-format-mysql
// @ref: http://stackoverflow.com/questions/2215354/php-date-format-when-inserting-into-datetime-in-mysql
// @ref: http://stackoverflow.com/questions/6590119/inserting-and-retriving-dates-and-timestamps-in-mongodb-using-php
var_dump(date_default_timezone_get()); // Get the server timezone.
date_default_timezone_set('Asia/Kuala_Lumpur');
$current = date('Y-m-d H:i:s');

// Insert a current date.
$timestamp = new MongoDate(strtotime($current));
var_dump($timestamp);

// To retrieve your date.
var_dump(date('Y-M-d h:i:s', $timestamp->sec));

//or,
// @ref: http://stackoverflow.com/questions/16792481/inserting-a-date-in-mongodb
$dt = new DateTime(date('Y-M-d h:i:s'), new DateTimeZone('Asia/Kuala_Lumpur'));
$ts = $dt->getTimestamp();
$today = new MongoDate($ts);
var_dump($today);

// To retrieve your date.
var_dump(date('Y-M-d h:i:s', $today->sec));

// Select a collection.
$collection = $db->selectCollection("users");
var_dump($collection);

// Create unique index on the field to prevent duplications.
$collection->createIndex(array('username' => 1), array('unique' => 1));

$document = array(
    "title" => "Mr",
    "fullname" => "John C",
    "username" => "user1",
    "age" => 20,
    "created_on" => $today->sec
);
//$collection->insert($document);

try {
    $collection->insert($document);
} catch(MongoCursorException $e) {
    echo $e;
}

// Find all rows.
$cursor = $collection->find();
var_dump($cursor);
var_dump(iterator_to_array($cursor));

// Count total.
var_dump($cursor->count());

// Limit.
//$cursor->limit(2);
//var_dump($cursor->count(true));

// iterate cursor to display title of documents
foreach ($cursor as $key => $document) {

    //var_dump($document);
    if(isset($document["title"])) echo $key. ' = '. $document["username"] . "<br/>";
}
// Get id of new document.
$id = $document['_id'];
var_dump($id);


$document = array(
    "title" => "Mr",
    "fullname" => "John C",
    "username" => "user1",
    "age" => 20
);

try {

    // Update a document.
    $result = $collection->update(
        array(
            "_id" => new MongoId($id)
        ),
        array(
            '$set' => $document
        )
    );
    var_dump($result);

} catch(MongoCursorException $e) {
    echo $e;
}

// one record.
$result = $collection->findOne(array("_id" => new MongoId($id)));
var_dump($result);

// Remove a document.
//$result = $collection->remove(array('_id' => new MongoId($id)));
//var_dump($result);

// Remove all.
//$collection->remove();
