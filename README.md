# MongoDB

## Windows

1. Download MongoDB at https://www.mongodb.org/downloads?_ga=1.203681689.88690536.1437615615

2. If you are running any edition of Windows Server 2008 R2 or Windows 7, install a hotfix to resolve an issue with memory mapped files on Windows - https://support.microsoft.com/en-us/kb/2731284.

3. Set up the MongoDB environment using the following commands from a Command Prompt,

    `md \data\db`

4. Start MongoDB from the Command Prompt,

    `C:\mongodb\bin\mongod.exe`

5. Connect to MongoDB - open another Command Prompt (MongoDB Shell),

    `C:\mongodb\bin\mongo.exe`

6. Show all database,

    `show dbs`

7. Define a database name,

    `use [your-db-name]`

8. Define a collection named “users“, and save a dummy document(value) inside,

    ```
    db.users.save( {username:"bla"} )
    db.users.find().pretty()
    { "_id" : ObjectId("4dbac7bfea37068bd0987573"), "username" : "bla" }
    ```

    Ref:

    * http://docs.mongodb.org/getting-started/shell/tutorial/install-mongodb-on-windows/
    * http://docs.mongodb.org/getting-started/shell/client/
    * http://www.mkyong.com/mongodb/how-to-create-database-or-collection-in-mongodb/
    * http://www.tutorialspoint.com/mongodb/index.htm
    * http://www.tutorialspoint.com/mongodb/mongodb_overview.htm

## Linux

1. Install mongodb server:

    `$ sudo apt-get install mongodb-server`

    Check version:

    `$ mongo --version` or `$ mongod --version`

    Check status:

    `$ sudo service mongodb status`

    Ref:
    * https://docs.mongodb.com/manual/tutorial/install-mongodb-on-ubuntu/

5. Start MongoDB from the Command Prompt,

    `$ mongod`

6. Connect to MongoDB - open another Command Prompt (MongoDB Shell),

    `> mongo`

7. Show all database,

    `show dbs`

# MongoDB + PHP

## Windows

1. Installing the PHP driver for MongoDB - download the correct driver for your environment from https://s3.amazonaws.com/drivers.mongodb.org/php/index.html

2. Unzip and add the appropriate php_mongo.dll file to your PHP extensions directory (usually the ext folder in your PHP installation).

3. Add to your php.ini,

    `extension=php_mongo.dll`

4. Restart your web server (Apache, IIS, etc.) for the change to take effect.

5. Open up your text editor and add the following code to a new file:<?php phpinfo();?>, save the file as phpinfo.php inside the DocumentRoot of the Apache web server (htdocs) and open the PHP script in the browser. If you see mongo in the PHP info, the installation was successful.

6. Select a database,

    ```
    $m = new MongoClient(); // connect
    $db = $m->selectDB("example");
    ```

    or,

    ```
    $m = new MongoClient();
    $db = $m->example;
    ```

7. Create a collection/ table,

    ```
    // connect to mongodb
    $m = new MongoClient();

    // select a database
    $db = $m->exmaple;
    $collection = $db->createCollection("users");
    ```

8. Insert a document/ row,

    ```
    // connect to mongodb
    $m = new MongoClient();

    // select a database
    $db = $m->exmaple;

    $collection = $db->users;

    $document = array(
        "title" => "Mr",
        "fullname" => "John C",
        "username" => "user1",
        "age" => 20
    );
    $collection->insert($document);
    ```

9. Find all documents,

    ```
    $cursor = $collection->find();
    ```

10. Update a document,

    ```
    $collection->update(array("title"=>"MongoDB"), array('$set'=>array("title"=>"MongoDB Tutorial")));
    ```

10. Delete a document,

    ```
    $collection->remove(array("title"=>"MongoDB Tutorial"), false);
    ```

    Ref:

    * http://docs.mongodb.org/ecosystem/drivers/php/
    * http://www.sitepoint.com/building-simple-blog-app-mongodb-php/
    * http://www.tutorialspoint.com/mongodb/mongodb_php.htm
    * http://php.net/manual/en/mongo.connecting.php
    * http://php.net/manual/en/book.mongo.php
    * http://php.net/manual/en/class.mongocursor.php
    * http://php.net/manual/en/class.mongocollection.php
    * http://www.tutorialspoint.com/mongodb/mongodb_query_document.htm

## Linux

1. First, to get this thing going, we'll need to resolve some dependencies. To do that, run this command from the terminal:

    `$ sudo apt-get install php-pear php5-dev`

2. Next, you should easily be able to install the driver with the following command:

    `$ sudo pecl install mongodb`

    Result:

    ```
    Build process completed successfully
    Installing '/usr/lib/php5/20121212/mongodb.so'
    install ok: channel://pecl.php.net/mongodb-1.1.7
    configuration option "php_ini" is not set to php.ini location
    You should add "extension=mongodb.so" to php.ini
    ```

3. Config php.ini

    $ locate php.ini

    $ sudo vim /path/to/php.ini

    Add `extension=mongodb.so` at the end of php.ini

    type :wq to save the file and leave vim.

    After you've saved your new .ini file, restart Apache `sudo /etc/init.d/apache2 restart` and verify that the extension is loading with phpinfo().

    Ref:
    * http://zacvineyard.com/blog/2013/02/the-easy-way-to-install-the-mongodb-php-driver-on-ubuntu-1204
    * http://stackoverflow.com/questions/34059895/warning-pecl-mongo-is-deprecated-in-favor-of-channel-mongodb
    * https://docs.mongodb.com/manual/tutorial/install-mongodb-on-ubuntu/
    * http://www.liquidweb.com/kb/how-to-install-the-mongodb-php-driver-extension-on-centos-6/

**Notes:**

1.

The mongodb driver provides a minimal API for core driver functionality, but no classes such as MongoClient. The MongoDB PHP library provides a high-level abstraction around this lower-level driver, e.g. a Client class, and can be installed with composer.

You may get this error **Class 'MongoClient' not found** after the mongodb installation above, when you try to connect to mongo with `new MongoClient()`

    Solution:

    1. Open your Ubuntu Software Centre and search for `mongo driver`.
    2. Select MongoDB Database Driver php5-mongo and install it.
    3. Restart Apache: `sudo /etc/init.d/apache2 restart`

# MongoDB Authentication

1. Create a new user,

    ```
    db.createUser(
      {
        user: "root",
        pwd: "password",
        roles: [
            { role: "userAdmin", db: "mydb" }
        ]
      }
    )
    ```

    Add roles to the user,

    ```
    db.grantRolesToUser(
        "root",
        [
          { role: "readWrite", db: "mydb" }
        ]
    )
    ```

    Select the user,

    `db.getUser("root")`

    List users,

    `db.getUsers()`

    Drop a user,

    ```
    db.runCommand( {
       dropUser: "reportUser1",
       writeConcern: { w: "majority", wtimeout: 5000 }
    } )
    ```

2. Start MongoDB from the Command Prompt,

    `C:\mongodb\bin\mongod.exe --auth`

3. Connect to MongoDB - open another Command Prompt (MongoDB Shell),

    `C:\mongodb\bin\mongo.exe`

4. Authenticate the user,

    `db.auth("root", "password")`

## Ref:

    ** http://docs.mongodb.org/manual/reference/method/js-user-management/

# MongoDB Authentication + PHP

    1. Provide the user details in `MongoClient`,

    ```
    $m = new MongoClient("mongodb://localhost", array(
        "db" => "mydb",
        "username" => "root",
        "password" => "password"
    ));
    ```

## Ref:

    ** http://php.net/manual/en/mongo.connecting.auth.php

# MongoDB Unique Index

1. MongoDB allows you to specify a unique constraint on an index. These constraints prevent applications from inserting documents that have duplicate values for the inserted fields.

2. MongoDB cannot create a unique index on the specified index field(s) if the collection already contains data that would violate the unique constraint for the index.

3. To create a unique index, consider the following prototype:

    `db.collection.createIndex( { a: 1 }, { unique: true } )`

    Example,

    `db.accounts.createIndex( { "tax-id": 1 }, { unique: true } )`

    Ref:

    * http://docs.mongodb.org/manual/tutorial/create-a-unique-index/

# MongoDB Unique Index + PHP

1. Just set the targeted column to `'unique' => 1`,

    Example,

    `$collection->createIndex(array('username' => 1), array('unique' => 1));`

    Ref:

    * http://php.net/manual/en/mongocollection.createindex.php
    * http://stackoverflow.com/questions/31601542/php-mongodb-insert-unique-values-only
